from django import forms
from django.contrib.auth.models import User
from validate_email import validate_email
from password_strength import PasswordPolicy
from core.models import Profile

BOOTSTRAP_FORM_CONTROL = 'form-control'


class SignUpForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password')

        widgets = {
            'username': forms.TextInput(attrs={'class': BOOTSTRAP_FORM_CONTROL, 'placeholder': 'Username'}),
            'email': forms.EmailInput(attrs={'class': BOOTSTRAP_FORM_CONTROL, 'placeholder': 'Email address'}),
            'password': forms.PasswordInput(attrs={'class': BOOTSTRAP_FORM_CONTROL, 'placeholder': 'Password'})
        }

    def clean_username(self):
        username = self.cleaned_data.get('username')

        if username is None:
            raise forms.ValidationError('This field is required')

        if User.objects.filter(username__exact=username).exists():
            raise forms.ValidationError('This username has already been taken :(')
        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if email is None:
            raise forms.ValidationError('This field is required')
        if User.objects.filter(email__iexact=email).exists():
            raise forms.ValidationError('This email has already been taken :(')

        '''
            instead you can use validate_email(email=email, check_mx=True) for smtp server verification
            or validate_email(email=email, verify=True) for smtp and email address[really exists] verification
        '''
        if not validate_email(email=email):
            raise forms.ValidationError('Verify your email address is a right one')

        return email

    def clean_password(self):
        password = self.cleaned_data.get('password')

        if password is None:
            raise forms.ValidationError('This field is required')

        password_policy = PasswordPolicy.from_names(
            length=8,
            uppercase=1,
            numbers=1,
            special=0,
            nonletters=0
        )

        if len(password_policy.test(password)) > 0:
            raise forms.ValidationError('This password is not strong enough')
        return password

    def save(self, commit=True):
        user = super(SignUpForm, self).save(commit=False)
        password = self.cleaned_data.get('password')
        user.set_password(raw_password=password)
        user = super(SignUpForm, self).save()
        Profile.objects.create(user=user)  # associated a profile to user we can make it with signal too
        return user


class SignInForm(forms.Form):
    username_or_email = forms.CharField(max_length=255,
                                        widget=forms.TextInput(attrs={'class': BOOTSTRAP_FORM_CONTROL,
                                                                      'placeholder': 'Username or email'}))
    password = forms.CharField(max_length=255, widget=forms.PasswordInput(
        attrs={'class': BOOTSTRAP_FORM_CONTROL, 'placeholder': 'password'}))


class PasswordResetTokenForm(forms.Form):
    username_or_email = forms.CharField(max_length=255,
                                        widget=forms.TextInput(attrs={'class': BOOTSTRAP_FORM_CONTROL,
                                                                      'placeholder': 'Username or email'}))


class PasswordReplaceForm(forms.Form):
    password1 = forms.CharField(max_length=144, min_length=8,
                                label='Password',
                                widget=forms.PasswordInput(
                                    attrs={'class': BOOTSTRAP_FORM_CONTROL, 'placeholder': 'Password'}))
    password2 = forms.CharField(max_length=144, min_length=8,
                                label='Confirm password',
                                widget=forms.PasswordInput(
                                    attrs={'class': BOOTSTRAP_FORM_CONTROL, 'placeholder': 'Confirm password'}))

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password2 is not None and password1 == password2:
            return password2
        raise forms.ValidationError('Password do not match')
