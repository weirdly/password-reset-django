from django.utils.crypto import salted_hmac
from django.utils.http import int_to_base36
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from core.models import Profile


class OneTimePasswordResetTokenGenerator(PasswordResetTokenGenerator):

    def _make_hash_value(self, user, timestamp):

        if user.is_superuser:
            if not hasattr(user, 'profile'):
                Profile.objects.create(user=user)

        login_timestamp = '' if user.last_login is None else user.last_login.replace(microsecond=0, tzinfo=None)
        return (
                str(user.pk) + user.username + user.email +
                str(timestamp) + str(user.profile.last_reset_date) +
                user.password + str(login_timestamp)
        )

    def _make_token_with_timestamp(self, user, timestamp):
        # timestamp is number of days since 2001-1-1.  Converted to
        # base 36, this gives us a 3 digit string until about 2121
        ts_b36 = int_to_base36(timestamp)
        hash_string = salted_hmac(
            self.key_salt,
            self._make_hash_value(user, timestamp),
            secret=self.secret,
        ).hexdigest()  # just let result to be full length
        return "%s-%s" % (ts_b36, hash_string)
