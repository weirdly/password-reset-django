from django.urls import path
from core import views


urlpatterns = [
    path('sign-up/', views.SignUpView.as_view(), name='sign-up'),
    path('sign-in/', views.SignInView.as_view(), name='sign-in'),
    path('logout/', views.SignOutView.as_view(), name='logout'),
    path('reset/password', views.PasswordResetTokenSenderView.as_view(), name='reset-password'),
    path('token/check/<str:username>/<str:token>', views.PasswordResetTokenHandlerView.as_view(), name='check-token'),
    path('home/', views.HomeView.as_view(), name='home'),
]