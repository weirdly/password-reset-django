from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.utils import timezone
from django.views.generic import TemplateView, FormView

from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

from core.forms import SignUpForm, SignInForm, PasswordResetTokenForm, PasswordReplaceForm
from core.tokens import OneTimePasswordResetTokenGenerator
from password_reset_app.settings import EMAIL_HOST
from smtplib import SMTPException

TOKEN_GENERATOR = OneTimePasswordResetTokenGenerator()


class SignUpView(FormView):
    form_class = SignUpForm
    template_name = 'core/sign_up.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('home'))
        return super(SignUpView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, message="Your account has been successfully created",
                             extra_tags="alert alert-success")
            return HttpResponseRedirect(reverse_lazy('sign-in'))
        return render(request, self.template_name, context={'form': form})


class SignInView(FormView):
    form_class = SignInForm
    template_name = 'core/sign_in.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('home'))
        return super(SignInView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            username_or_email = form.cleaned_data.get('username_or_email')
            password = form.cleaned_data.get('password')

            pretended_user = User.objects.filter(
                Q(username__exact=username_or_email) |
                Q(email__iexact=username_or_email)
            ).first()
            if pretended_user is None:
                messages.error(request, message="This email or username is not associated with any account",
                               extra_tags="alert alert-danger")
                return render(request, self.template_name, context={'form': form})

            username = pretended_user.username
            user = authenticate(request, username=username, password=password)

            if user is None:
                messages.error(request, message="Username or password is incorrect", extra_tags="alert alert-danger")
                return render(request, self.template_name, context={'form': form})

            login(request, user)
            return HttpResponseRedirect(reverse_lazy('home'))

        return render(request, self.template_name, context={'form': form})


class SignOutView(LoginRequiredMixin, TemplateView):
    template_name = 'core/sign_out.html'

    login_url = reverse_lazy('sign-in')
    redirect_field_name = 'next'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(SignOutView, self).get(request, *args, **kwargs)


class PasswordResetTokenSenderView(FormView):
    form_class = PasswordResetTokenForm
    template_name = 'core/password_reset_token.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('home'))
        return super(PasswordResetTokenSenderView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            username_or_email = form.cleaned_data.get('username_or_email')
            user = User.objects.filter(
                Q(username__exact=username_or_email) | Q(email__iexact=username_or_email)).first()

            if user is None:
                # form.add_error('field', 'error') but I do not handle form error on html page so
                msg = "This username or email is not associated to an account"
                messages.error(request, message=msg, extra_tags='alert alert-danger')
                return render(request, self.template_name, context={'form': form})

            user_token = TOKEN_GENERATOR.make_token(user)
            email_context = {
                'domain': request.get_host(),
                'token': user_token,
                'username': user.username,
                'full_name': user.get_full_name,
            }
            subject = 'Reset your Django demo password.'
            subject = ''.join(subject.splitlines())

            from_email, to_email = EMAIL_HOST, user.email
            html_content = get_template('core/mails/password_reset_token_mail.html')
            html_content.render(email_context)
            email_message = EmailMultiAlternatives(subject, from_email, [to_email])
            email_message.attach_alternative(html_content, 'text/html')

            print(f'http://localhost:8000/token/check/{user}/{user_token}')
            try:
                email_message.send()
                msg = "Email has been successfully sent check your inbox :)"
                messages.success(request, message=msg, extra_tags='alert alert-success')
                return render(request, self.template_name, context={'form': form})
            except SMTPException:
                messages.error(request, message='Oops something goes wrong', extra_tags='alert alert-danger')
                return render(request, self.template_name, context={'form': form})


class PasswordResetTokenHandlerView(FormView):
    form_class = PasswordReplaceForm
    template_name = 'core/password_replace.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('home'))
        return super(PasswordResetTokenHandlerView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            username, token = kwargs.get('username'), kwargs.get('token')

            user = get_object_or_404(User, username=username)

            token_is_valid = TOKEN_GENERATOR.check_token(user=user, token=token)
            if token_is_valid:
                password2 = form.cleaned_data.get('password2')
                user.set_password(raw_password=password2)
                user.profile.last_reset_date = timezone.now()
                user.save()
                messages.success(request, message='Your password has been successfully changed',
                                 extra_tags='alert alert-success')
                return render(request, self.template_name, context={'form': form})

            messages.error(request, message='This token is not valid generate a new one',
                           extra_tags='alert alert-danger')
            return render(request, self.template_name, context={'form': form})

        return render(request, self.template_name, context={'form': form})


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'core/index.html'

    login_url = reverse_lazy('sign-in')
    redirect_field_name = 'next'  # can be import from django.contrib.auth.mixins import REDIRECT_FIELD_NAME
